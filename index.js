var fs = require('fs')
const express = require('express')
const bodyParser = require('body-parser')
var helmet = require('helmet')
require('dotenv').config()
// var crypto = require('crypto');
var cors = require('cors')
var request = require('request')
var Q = require('q')
var moment = require('moment')
var logger = require('tracer').colorConsole()
var RateLimit = require('express-rate-limit')
// var https = require('https');
var https = require('http')
var async = require('async')
var request = require('request');
var joiSchemaValidator = require('./joiSchemaValidator')
var Base64 = require('js-base64').Base64;
const uuid = require('uuid/v4');

const app = express()

var limiter = new RateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 50, // limit each IP to 100 requests per windowMs
    delayMs: 1000 // disable delaying - full speed until the max limit is reached
})


app.disable('x-powered-by')
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())
app.use(cors())
app.use(limiter)
app.use(helmet())
app.set('json spaces', 2) // use only in development

// const options = {
//     key: fs.readFileSync('/usr/local/ssl/key/private.key', 'utf8'),
//     cert: fs.readFileSync('/usr/local/ssl/crt/public.crt', 'utf8'),
//     ca: fs.readFileSync('/usr/local/ssl/crt/intermediate.crt', 'utf8')
// };

var httpsServer = https.createServer(app)
// var httpsServer = https.createServer(options, app);
httpsServer.listen(8001, function () {
    console.log('SERVER RUNNING AT 8001')
})

var curl = require('curlrequest');

const EA_BASE_URL = "http://localhost/ea/index.php";
const EA_PROVIDERS_URL = EA_BASE_URL + "/api/v1/providers";
const EA_CUSTOMERS_URL = EA_BASE_URL + "/api/v1/customers";
const EA_USERNAME = process.env.EA_USERNAME;
const EA_PASSWORD = process.env.EA_PASSWORD;


var auth = Base64.encode(EA_USERNAME + ':' + EA_PASSWORD)

const EA_HEADERS = {
    'cache-control': 'no-cache',
    authorization: 'Basic ' + auth,
    'content-type': 'application/json'
}

app.post('/add-provider', function (req, response) {
    var params = req.body;
    var isValidParams = joiSchemaValidator.providerSchema.validate(params);
    logger.log(isValidParams);
    if (!isValidParams.error) {
        if (params.username && params.password) {
            saveProvider(params, function (error, body) {
                if (error) {
                    response.status(400).send(errorResponse());
                } else {
                    response.status(200).send(okResponse(body));
                }
            });
        } else {
            var provider = params;
            provider.username = provider.uniqid;
            provider.password = uuid();
            saveProvider(provider, function (error, body) {
                if (error) {
                    response.status(400).send(errorResponse());
                } else {
                    response.status(200).send(okResponse(body));
                }
            });
        }
    } else {
        response.status(400).send(errorResponse());
    }
});

app.post('/add-customer', function (req, response) {
    var params = req.body;
    var isValidParams = joiSchemaValidator.customerSchema.validate(params);
    logger.log(isValidParams);
    if (!isValidParams.error) {
        if (params.username && params.password) {
            saveCustomer(params, function (error, body) {
                if (error) {
                    response.status(400).send(errorResponse());
                } else {
                    response.status(200).send(okResponse(body));
                }
            });
        } else {
            var customer = params;
            customer.username = customer.uniqid;
            customer.password = uuid();
            saveCustomer(customer, function (error, body) {
                if (error) {
                    response.status(400).send(errorResponse());
                } else {
                    response.status(200).send(okResponse(body));
                }
            });
        }
    } else {
        response.status(400).send(errorResponse());
    }
});


function decodeUserData(user) {
    user.settings = {
        username: user.username,
        password: user.password,
        googleSync: 0,
        notifications: 1
    }
    delete user.username;
    delete user.password;
    return user;
}

function saveProvider(provider, callback) {
    saveUserAs('PROVIDER', provider, callback);
}

function saveCustomer(customer, callback) {
    saveUserAs('CUSTOMER', customer, callback);
}

function saveUserAs(user_type, user, callback) {
    user = decodeUserData(user);
    var url = EA_CUSTOMERS_URL;
    if (user_type == 'PROVIDER') {
        url = EA_PROVIDERS_URL
    }
    var options = {
        method: 'POST',
        url: url,
        headers: EA_HEADERS,
        body: user,
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            logger.log(error);
            callback(error, null, null);
        } else {
            callback(null, body);
        }
    });
}

function errorResponse() {
    return {
        status_code: 400,
        message: "Something went wrong!"
    }

}

function okResponse(data) {
    return {
        status_code: 200,
        data: data,
        message: "OK"
    }
}

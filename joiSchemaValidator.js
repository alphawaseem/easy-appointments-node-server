const joi = require('joi');

const providerSchema = joi.object().keys({
  firstName: joi.string().required(),
  lastName: joi.string().required(),
  email: joi.string().email().required(),
  phone: joi.string().regex(/\+\d{1,2}-?\d{8,12}/).required(),
  address: joi.string(),
  city: joi.string(),
  state: joi.string(),
  zip: joi.string(),
  services: joi.array().min(1).items(joi.number().integer()),
  username: joi.string().min(6).max(15),
  uniqid: joi.string(),
  password: joi.string(),
  access_token: joi.string()
}).without('username', 'uniqid').without('password', 'access_token')

const customerSchema = joi.object().keys({
  firstName: joi.string().required(),
  lastName: joi.string().required(),
  email: joi.string().email().required(),
  phone: joi.string().regex(/\+\d{1,2}-?\d{8,12}/).required(),
  address: joi.string().required(),
  city: joi.string().required(),
  state: joi.string().required(),
  zip: joi.string().required(),
  services: joi.array().min(1).items(joi.number().integer()),
  username: joi.string().min(6).max(15),
  uniqid: joi.string(),
  password: joi.string(),
  access_token: joi.string()
}).without('username', 'uniqid').without('password', 'access_token')

module.exports.providerSchema = providerSchema;
module.exports.customerSchema = customerSchema;
